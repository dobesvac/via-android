/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package android.via.dropfolder;

import android.app.Activity;
import android.os.Bundle;
import android.via.dropfolder.controller.AccountController;
import android.via.dropfolder.crate.UserCrate;
import android.via.dropfolder.validator.AccountValidator;
import android.via.dropfolder.validator.container.Error;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import java.util.List;

/**
 *
 * @author venca
 */
public class RegisterActivity extends Activity {
    
    private Button register;
    private Button cancel;
    
    private AccountController manager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.register);
        
        manager=new AccountController(this);
        
        register=(Button) this.findViewById(R.id.registerBtn);
        register.setOnClickListener(new Register());
        
        cancel=(Button) this.findViewById(R.id.registerCancelBtn);
        cancel.setOnClickListener(new Cancel());
    }
    
    private class Register implements View.OnClickListener {
               
        public void onClick(View view) {
            EditText user=(EditText) RegisterActivity.this.findViewById(R.id.registerUserInput);
            EditText psswd=(EditText) RegisterActivity.this.findViewById(R.id.registerPasswordInput);
            EditText psswdCheck=(EditText) RegisterActivity.this.findViewById(R.id.registerPasswordInputCheck);
            
            AccountValidator validator=new AccountValidator(RegisterActivity.this, 3);
            validator.required(user);
            validator.required(psswd);
            validator.sameValue(psswd, psswdCheck, RegisterActivity.this.getString(R.string.password_check_error));
            
            if(validator.isValid()){
                UserCrate crate=new UserCrate();
                crate.username=user.getText().toString();
                crate.password=psswd.getText().toString();
                manager.register(crate);
            }else{
                List<Error> errors=validator.getErrorList();
                for (Error e : errors) {
                    e.getField().setError(e.getMessage());
                }
            }
        }
    }
    
    private class Cancel implements View.OnClickListener {
               
        public void onClick(View view) {
            RegisterActivity.this.finish();
        }
           
    }
    
}
