/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package android.via.dropfolder;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.via.dropfolder.controller.FileController;
import android.via.dropfolder.crate.FileCrate;
import android.via.dropfolder.preferences.UserPreferences;
import android.via.dropfolder.validator.Validator;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import java.util.List;

/**
 *
 * @author venca
 */
public class AddFolderDialog extends DialogFragment {
    
    private AlertDialog.Builder dialog;
    private AlertDialog alert;
    protected EditText input;
    
    protected FileController manager;
    protected String baseUrl;
    
    protected Activity activity;
        
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        manager=((MainActivity) this.getActivity()).getAsyncFileManager();
    }
    
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog=new AlertDialog.Builder(this.getActivity());
       // dialog.setTitle(R.string.add_folder);
        this.activity=this.getActivity();
        
        this.baseUrl=UserPreferences.getFileSystemUrl(activity);
        
        LayoutInflater inflater=this.getActivity().getLayoutInflater();
        
        dialog.setView(inflater.inflate(R.layout.add_folder, null));
                     
        dialog.setPositiveButton(R.string.create, null);
        dialog.setNegativeButton(R.string.cancel, new Cancel());      
        alert=dialog.create();
        
        alert.setOnShowListener(new DialogInterface.OnShowListener() {

            public void onShow(DialogInterface dialog) {
                Button b=alert.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new Submit());
            }
            
        });
        
        return alert;
    }
    
    private void addFolder(String url, FileCrate crate){
        manager.addFolder(url, crate, true);
    }
    
    private class Submit implements View.OnClickListener {

        public void onClick(View view) {
            
            input=(EditText) alert.findViewById(R.id.addFolderName);
            String name=input.getText().toString();
            
            Validator validator=new Validator(activity, 1);
            validator.required(input);
            
            if(validator.isValid()){
                FileCrate crate=new FileCrate();
                crate.name=name;
                addFolder(manager.getParent().getUrl(), crate);
                alert.dismiss();
            }else{
                List<android.via.dropfolder.validator.container.Error> errors=validator.getErrorList();
                for (android.via.dropfolder.validator.container.Error e : errors) {
                    e.getField().setError(e.getMessage());
                }
            }            
        }
    }
    
    private class Cancel implements DialogInterface.OnClickListener {

        public void onClick(DialogInterface dialog, int which) {
            
        }
        
    }
    
}
