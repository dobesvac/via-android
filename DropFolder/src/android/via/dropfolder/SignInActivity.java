/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package android.via.dropfolder;

import android.accounts.AccountAuthenticatorActivity;
import android.content.Intent;
import android.os.Bundle;
import android.via.dropfolder.controller.AccountController;
import android.via.dropfolder.crate.UserCrate;
import android.via.dropfolder.preferences.UserPreferences;
import android.via.dropfolder.validator.AccountValidator;
import android.via.dropfolder.validator.container.Error;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import java.util.List;

/**
 *
 * @author venca
 */
public class SignInActivity extends AccountAuthenticatorActivity {

    private Button sign;
    private Button register;
    
    private AccountController manager;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_in);
        
        if(UserPreferences.isLogged(this)){
            Intent intent=new Intent(this, MainActivity.class);
            this.startActivity(intent);
        }
        
        manager=new AccountController(this);
        
        sign=(Button) this.findViewById(R.id.signInBtn);
        sign.setOnClickListener(new SignIn());
        
        register=(Button) this.findViewById(R.id.signRegisterBtn);
        register.setOnClickListener(new Register());
                
    }
        
    private class SignIn implements View.OnClickListener {
               
        public void onClick(View view) {
            EditText user=(EditText) SignInActivity.this.findViewById(R.id.signUserInput);
            EditText psswd=(EditText) SignInActivity.this.findViewById(R.id.signPasswordInput);
            
            AccountValidator validator=new AccountValidator(SignInActivity.this, 2);
            validator.required(user);
            validator.required(psswd);
            
            if(validator.isValid()){
                UserCrate crate=new UserCrate();
                crate.username=user.getText().toString();
                crate.password=psswd.getText().toString();
                manager.signIn(crate);
            }else{
                List<Error> errors=validator.getErrorList();
                for (Error e : errors) {
                    e.getField().setError(e.getMessage());
                }
            }
        }
           
    }
    
    private class Register implements View.OnClickListener {
               
        public void onClick(View view) {
            Intent intent=new Intent(SignInActivity.this, RegisterActivity.class);
            SignInActivity.this.startActivity(intent);
        }
           
    }
    
}
