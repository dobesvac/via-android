/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package android.via.dropfolder.validator.container;

import android.widget.EditText;

/**
 *
 * @author venca
 */
public class Error {
    EditText field;
    String message;
 
    public Error(EditText field, String message){
        this.field=field;
        this.message=message;
    }

    public EditText getField() {
        return field;
    }

    public void setField(EditText field) {
        this.field = field;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    
    
}
