/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package android.via.dropfolder.validator;

import android.via.dropfolder.R;
import android.app.Activity;
import android.via.dropfolder.validator.container.Error;
import android.widget.EditText;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author venca
 */
public class Validator {
    
    protected List<Error> errors;
    protected Activity activity;
    
    public Validator(Activity activity, int size){
        this.errors=new ArrayList<Error>(size);
        this.activity=activity;
    }
    
    public void required(EditText field){
        required(field, this.activity.getString(R.string.validate_required));
    }
    
    public void required(EditText field, String message){
        if(field.getText().toString().isEmpty()){
            this.errors.add(new Error(field, message));
        }
    }
    
    public void sameValue(EditText field1, EditText field2){
        sameValue(field1, field2, this.activity.getString(R.string.validate_check_field));
    }
    
    public void sameValue(EditText field1, EditText field2, String message){
        if(!field1.getText().toString().equals(field2.getText().toString())){
            this.errors.add(new Error(field2, message));
        }
    }
    
    public boolean isValid(){
        return errors.isEmpty();
    }
    
    public List<Error> getErrorList(){
        return errors;
    }
    
}
