/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package android.via.dropfolder.controller;

import android.app.Activity;
import android.via.dropfolder.R;
import android.via.dropfolder.adapter.AsyncAdapter;
import android.via.dropfolder.async.AsyncManager;
import android.widget.Toast;

/**
 *
 * @author venca
 */
public class Controller {
    
    protected Activity activity;
    
    public Controller(Activity activity){
        this.activity=activity;
    }
    
    protected void sendAsyncRequest(AsyncManager async, AsyncAdapter adapter, String url){
        if(async.isNetworkAvailable(activity)){
            async.setAdapter(adapter);
            async.execute(url);     
        }else{
            Toast.makeText(activity, activity.getString(R.string.no_connection), Toast.LENGTH_SHORT).show();
        }
    }
    
}
