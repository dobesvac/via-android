/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package android.via.dropfolder.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.via.dropfolder.MainActivity;
import android.via.dropfolder.R;
import android.via.dropfolder.adapter.AsyncAdapter;
import android.via.dropfolder.async.AsyncRegisterUser;
import android.via.dropfolder.async.AsyncSignInUser;
import android.via.dropfolder.crate.UserCrate;
import android.via.dropfolder.parser.JSONAccountParser;
import android.widget.Toast;

/**
 *
 * @author venca
 */
public class AccountController extends Controller {
    
    private String url;
        
    public AccountController(Activity activity){
        super(activity);
    }
    
    public void register(final UserCrate crate){
        url=activity.getString(R.string.url_account_register);
        AsyncRegisterUser async=new AsyncRegisterUser(crate);
        AsyncAdapter adapter=new AsyncAdapter(this.activity) {
            private ProgressDialog dialog;
            
            @Override
            public void onPreExecute(){
                dialog=ProgressDialog.show(activity, "", activity.getString(R.string.processing), true);
            }
            
            @Override
            public void onPostExecute(String result){
                dialog.cancel();
            }
            
            @Override
            public void onPostExecuteStatusCreated(String result){
                new AlertDialog.Builder(activity)
                .setMessage(R.string.register_success)
                .setNeutralButton(R.string.confirm, new DialogInterface.OnClickListener(){

                    public void onClick(DialogInterface dialog, int which) {
                        activity.finish();
                    }
                    
                }).show();
            }
            
            @Override
            public void onPostExecuteStatusConflict(String result){
                String message=activity.getString(R.string.register_user_exists);
                message=String.format(message, crate.username);
                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
            }
            
        };
        
        this.sendAsyncRequest(async, adapter, url);
    }
    
    public void signIn(final UserCrate crate){
        url=activity.getString(R.string.url_account_sign_in);
        AsyncSignInUser async=new AsyncSignInUser(crate);
        AsyncAdapter adapter=new AsyncAdapter(this.activity) {
            private ProgressDialog dialog;
            
            @Override
            public void onPreExecute(){
                dialog=ProgressDialog.show(activity, "", activity.getString(R.string.processing), true);
            }
            
            @Override
            public void onPostExecute(String result){
                dialog.cancel();
            }
            
            @Override
            public void onPostExecuteStatusOk(String result){
                JSONAccountParser parser=new JSONAccountParser(activity, crate.password, result);
                parser.parse();
                
                Intent intent=new Intent(activity, MainActivity.class);
                activity.startActivity(intent);
                Toast.makeText(activity, R.string.sign_in_success, Toast.LENGTH_SHORT).show();
            }
            
            @Override
            public void onPostExecuteStatusUnauthorized(String result){
                Toast.makeText(activity, R.string.sign_in_error, Toast.LENGTH_SHORT).show();
            }
              
        };
        
        this.sendAsyncRequest(async, adapter, url);
    }
    
}
