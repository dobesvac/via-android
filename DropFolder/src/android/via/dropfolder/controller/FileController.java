/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package android.via.dropfolder.controller;

import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;
import android.via.dropfolder.R;
import android.via.dropfolder.adapter.AsyncAdapter;
import android.via.dropfolder.adapter.FileAdapter;
import android.via.dropfolder.async.AsyncAddFile;
import android.via.dropfolder.async.AsyncAddFolder;
import android.via.dropfolder.async.AsyncDeleteFile;
import android.via.dropfolder.async.AsyncManager;
import android.via.dropfolder.async.AsyncFileList;
import android.via.dropfolder.browser.FileBrowser;
import android.via.dropfolder.crate.FileCrate;
import android.via.dropfolder.dao.AbstractFileDAO;
import android.via.dropfolder.dao.FileDAO;
import android.via.dropfolder.dao.FolderDAO;
import android.via.dropfolder.parser.JSONFileListParser;
import android.via.dropfolder.parser.JSONFileParser;
import android.via.dropfolder.service.FileService;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author venca
 */
public class FileController extends Controller {
    
    private List<AbstractFileDAO> files;
    private FolderDAO parent;
    private final FileService service;
    
    protected String actualUrl;
    
    private final TextView title;
    
    public FileController(Activity activity){
        super(activity);
        this.service=new FileService(activity);
        this.title=(TextView) activity.findViewById(R.id.mainTitle);
    }
    
    public void setFiles(List<AbstractFileDAO> files){
        this.files=files;
    }
    
    public List<AbstractFileDAO> getFiles(){
        return files;
    }
    
    public void setParent(FolderDAO parent){
        this.parent=parent;
    }
    
    public FolderDAO getParent(){
        return parent;
    }
    
    public String getActualUrl(){
        return actualUrl;
    }
    
    public void setActualUrl(String actualUrl){
        this.actualUrl=actualUrl;
    }
        
    public void loadFolderContentFromCache(FolderDAO parent){
        Log.i("folder", "load from cache");
        this.title.setText(parent.getName());
        ListView list=(ListView) activity.findViewById(R.id.mainList);
        
        service.open();
        files=service.getFolderContent(parent);
        service.close();

        FileAdapter adapter=new FileAdapter(activity, files);
        list.setAdapter(adapter);
        
        this.parent=parent;
        this.actualUrl=parent.getUrl();
    }
    
        
    public void loadFileFromCache(FileDAO file){
        try {
            FileBrowser.openFile(activity, file);
        } catch (IOException ex) {
            Log.i("folder", ex.getMessage());
        }
    }
        
    public void addFile(String url, FileCrate crate, boolean load){
        crate.type=FileDAO.TYPE;
        AsyncAddFile async=new AsyncAddFile(this.activity, crate);
        addFile(async, url, crate, R.string.add_file_success, load);
    }
    
    public void addFolder(String url, FileCrate crate, boolean load){
        crate.type=FolderDAO.TYPE;
        AsyncAddFolder async=new AsyncAddFolder(this.activity, crate);
        addFile(async, url, crate, R.string.add_folder_success,  load);
    }
    
    public void addFile(AsyncManager async, String url, final FileCrate crate, final int message, final boolean load){
        
        AsyncAdapter adapter=new AsyncAdapter(this.activity){
            private ProgressDialog dialog;
            
            @Override
            public void onPreExecute(){
                dialog=ProgressDialog.show(activity, "", activity.getString(R.string.processing), true);
            }
            
            @Override
            public void onPostExecute(String result){
                dialog.cancel();
            }
            
            @Override
            public void onPostExecuteStatusCreated(String result){
                JSONFileParser parser=new JSONFileParser(this.activity, service, FileController.this.parent, result);
                parser.parseFolderContent();
                if(load) loadFolderContentFromCache(FileController.this.parent);
                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
            }
            
            @Override
            public void onPostExecuteStatusConflict(String result){
                String message=activity.getString(R.string.folder_exists);
                message=String.format(message, crate.name);
                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
            }
        };
        this.sendAsyncRequest(async, adapter, url);
    }
    
    
    public void deleteFolder(String url, final String id, final boolean load){
        deleteFile(url, id, R.string.delete_folder_success, load);
    }
    
    public void deleteFile(String url, final String id, final boolean load){
        deleteFile(url, id, R.string.delete_file_success, load);
    }
    
    public void deleteFile(String url, final String id, final int message, final boolean load){
        AsyncDeleteFile async=new AsyncDeleteFile(this.activity);
        AsyncAdapter adapter=new AsyncAdapter(this.activity){
            private ProgressDialog dialog;
            
            @Override
            public void onPreExecute(){
                dialog=ProgressDialog.show(this.activity, "", this.activity.getString(R.string.processing), true);
            }
            
            @Override
            public void onPostExecute(String result){
                dialog.cancel();
            }
            
            @Override
            public void onPostExecuteStatusNoContent(String result){
                service.open();
                service.deleteFile(id);
                service.close();
                if(load) loadFolderContentFromCache(parent);
                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
            }
            
        };
        this.sendAsyncRequest(async, adapter, url);
    }
    
    public void loadFolderContentFromServer(final String url){
        AsyncFileList async=new AsyncFileList(this.activity);
        AsyncAdapter adapter=new AsyncAdapter(this.activity){
            
            private ProgressDialog dialog;

            @Override
            public void onPreExecute(){
                dialog=ProgressDialog.show(this.activity, "", this.activity.getString(R.string.loading), true);
            }

            @Override
            public void onPostExecute(String result){
                dialog.cancel();
            }

            @Override
            public void onPostExecuteStatusOk(String result){
                JSONFileListParser parser=new JSONFileListParser(this.activity, service, result);
                parser.parse();
                loadFolderContentFromCache(parser.getParent());
            }

        };
        this.sendAsyncRequest(async, adapter, url);
    }
    
    public void loadFileFromServer(final String url){
        AsyncFileList async=new AsyncFileList(this.activity);
        AsyncAdapter adapter=new AsyncAdapter(this.activity){
            
            private ProgressDialog dialog;

            @Override
            public void onPreExecute(){
                dialog=ProgressDialog.show(this.activity, "", this.activity.getString(R.string.loading), true);
            }

            @Override
            public void onPostExecute(String result){
                dialog.cancel();
            }

            @Override
            public void onPostExecuteStatusOk(String result){
                JSONFileParser parser=new JSONFileParser(this.activity, service, parent, result);
                parser.parseFile();
                FileDAO file=(FileDAO) parser.getFile();
                loadFileFromCache(file);        
            }

        };
        this.sendAsyncRequest(async, adapter, url);
    }
}
