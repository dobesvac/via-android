/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package android.via.dropfolder.dao;

import android.os.Parcel;
import android.os.Parcelable;

/**
 *
 * @author venca
 */
public abstract class AbstractFileDAO implements Parcelable {
        
    protected String id;
    protected String parentId;
    protected String name;
    protected String ending;
    protected String url;
    protected String parentUrl;
    
    public AbstractFileDAO(){
        
    }
    
    public AbstractFileDAO(Parcel in){
        this.setId(in.readString());
        this.setParentId(in.readString());
        this.setName(in.readString());
        this.setEnding(in.readString());
        this.setUrl(in.readString());
        this.setParentUrl(in.readString());
    }
    
    abstract public boolean isFolder();
    abstract public boolean isFile();
    abstract public String getType();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getParentId() {
        return parentId;
    }

    public void setParentId(String id) {
        this.parentId = id;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEnding() {
        if(ending==null) return null;
        return ending.toLowerCase();
    }

    public void setEnding(String ending) {
        this.ending = ending;
    }
    
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    
    public String getParentUrl() {
        return parentUrl;
    }

    public void setParentUrl(String parent) {
        this.parentUrl = parent;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.parentId);
        dest.writeString(this.name);
        dest.writeString(this.ending);
        dest.writeString(this.url);
        dest.writeString(this.parentUrl);
    }
}

