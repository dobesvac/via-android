/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package android.via.dropfolder.dao;

import android.content.Context;
import android.via.dropfolder.R;
import android.via.dropfolder.preferences.UserPreferences;

/**
 *
 * @author venca
 */
public class RootFolder extends FolderDAO {
    
    private Context context;
    
    public RootFolder(Context context){
        this.context=context;
    }
    
    @Override
    public String getId(){
        return "null";
    }
    
    @Override
    public String getParentId(){
        return "null";
    }
    
    @Override
    public String getName(){
        return context.getString(R.string.app_name);
    }
    
    @Override
    public boolean isRoot(){
        return true;
    }
    
    @Override
    public String getUrl(){
        return UserPreferences.getFileSystemUrl(context);
    }
    
    @Override
    public String getParentUrl(){
        return UserPreferences.getFileSystemUrl(context);
    }

    
}
