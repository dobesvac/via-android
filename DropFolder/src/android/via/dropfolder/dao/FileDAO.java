/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package android.via.dropfolder.dao;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.File;

/**
 *
 * @author venca
 */
public class FileDAO extends AbstractFileDAO {
    
    public static final String TYPE="file"; 
    
    private File data;
    private String contentType="";
    
    public FileDAO(){
        super();
    }
    
    public FileDAO(Parcel in){
        super(in);
    }

    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public boolean isFolder() {
        return false;
    }

    @Override
    public boolean isFile() {
        return true;
    }
    
    public boolean containsData(){
        return !(data==null);
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
       
    public void setFileData(File data){
        this.data=data;
    }
    
    public File getFileData(){
        return data;
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public FileDAO createFromParcel(Parcel in) {
            return new FileDAO(in); 
        }

        public FileDAO[] newArray(int size) {
            return new FileDAO[size];
        }
    };
    
}
