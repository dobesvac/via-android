/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package android.via.dropfolder.dao;

import android.os.Parcel;
import android.os.Parcelable;

/**
 *
 * @author venca
 */
public class FolderDAO extends AbstractFileDAO {
    
    private boolean parent=false;
    public static final String TYPE="directory"; 
    
    public FolderDAO(){
        super();
    }
    
    public FolderDAO(Parcel in){
        super(in);
    }

    public void setParent(boolean parent){
        this.parent=parent;
    }
    
    public boolean isParent(){
        return this.parent;
    }
    
    public boolean isRoot(){
        return this.name.equals("null");
    }
    
    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public boolean isFolder() {
        return true;
    }

    @Override
    public boolean isFile() {
        return false;
    }
    
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeByte((byte) (this.parent ? 1 : 0));
    }
    
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public FolderDAO createFromParcel(Parcel in) {
            return new FolderDAO(in); 
        }

        public FolderDAO[] newArray(int size) {
            return new FolderDAO[size];
        }
    };

}
