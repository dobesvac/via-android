/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package android.via.dropfolder.service;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.via.dropfolder.dao.AbstractFileDAO;
import android.via.dropfolder.dao.FileDAO;
import android.via.dropfolder.dao.FolderDAO;
import android.via.dropfolder.dao.RootFolder;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author venca
 */
public class FileService {
    
    private SQLiteDatabase database;
    private final Database helper;
    private final Context context;
    
    private final String[] columns;
    
    public FileService(Context c){
        this.columns=new String[]{
            Database.FILE_ID,
            Database.FILE_PARENT_ID,
            Database.FILE_NAME,
            Database.FILE_TYPE, 
            Database.FILE_URL,
            Database.FILE_PARENT_URL,
            Database.FILE_ENDING
        };
        this.context=c;
        this.helper=new Database(c);
    }
    
    public void open() throws SQLException {
        database=helper.getWritableDatabase();
    }

    public void close() {
        helper.close();
    }
    
    public AbstractFileDAO createFile(AbstractFileDAO file){
        if(file==null) return file;
        ContentValues values=new ContentValues(7);
        values.put(Database.FILE_ID, file.getId());
        values.put(Database.FILE_PARENT_ID, file.getParentId());
        values.put(Database.FILE_NAME, file.getName());
        values.put(Database.FILE_TYPE, file.getType());
        values.put(Database.FILE_URL, file.getUrl());
        values.put(Database.FILE_PARENT_URL, file.getParentUrl());
        values.put(Database.FILE_ENDING, file.getEnding());
        
        database.insert(Database.FILE_TABLE, Database.FILE_NAME, values);
        
        Log.i("folder", "creating file ID:"+file.getId());
        return file;
    }
    
    public AbstractFileDAO updateFile(AbstractFileDAO file){
        if(file==null) return null;
        ContentValues values=new ContentValues(7);
        if(file.getId()!=null) values.put(Database.FILE_ID, file.getId());
        if(file.getParentId()!=null) values.put(Database.FILE_PARENT_ID, file.getParentId());
        if(file.getName()!=null) values.put(Database.FILE_NAME, file.getName());
        if(file.getType()!=null) values.put(Database.FILE_TYPE, file.getType());
        if(file.getUrl()!=null) values.put(Database.FILE_URL, file.getUrl());
        if(file.getParentUrl()!=null) values.put(Database.FILE_PARENT_URL, file.getParentUrl());
        if(file.getEnding()!=null) values.put(Database.FILE_ENDING, file.getEnding());
        
        String[] args={file.getId()};
        int row=database.update(Database.FILE_TABLE, values, "id=?", args);        
        Log.i("folder", "updating file ID:"+file.getId());
        return getFile(file.getId());
    }
    
    public AbstractFileDAO saveOrUpdate(AbstractFileDAO file){
        String[] args={file.getId()};
        Cursor cursor=database.query(Database.FILE_TABLE, columns, "id=?", args, null, null, null);
        boolean exists=cursor.moveToFirst();
        if(exists){
            return updateFile(file);
        }else{
            return createFile(file);
        }
    }
    
    public void deleteFile(String file){
        String[] args={String.valueOf(file)};
        database.delete(Database.FILE_TABLE, "id=?", args);
        Log.i("folder", "deleting file ID: "+file);
    }
    
    public void deleteAllParentFiles(String parent){
        String[] args={String.valueOf(parent)};
        database.delete(Database.FILE_TABLE, "parent_id=?", args);
        Log.i("folder", "deleting all files");
    }
        
    public AbstractFileDAO getFile(String id){
        String[] args={id};
        Cursor cursor=database.query(Database.FILE_TABLE, columns, "id=?", args, null, null, null);
        boolean isNotEmpty=cursor.moveToFirst();
        if(isNotEmpty){
//            AbstractFileDAO file=cursorToFile(cursor);
            if(cursor.getString(3).equals(FileDAO.TYPE)){
                FileDAO file=cursorToFile(cursor);
                cursor.close();
            }else if(cursor.getString(3).equals(FolderDAO.TYPE)){
                FolderDAO file=cursorToFolder(cursor);
                cursor.close();
            return file;
            }
            return null;
        }else{
            cursor.close();
            return new RootFolder(this.context);
        }
    }
    
    public List<AbstractFileDAO> getFolderContent(FolderDAO folder){
        List<AbstractFileDAO> files=new ArrayList<AbstractFileDAO>(); 
        String[] args={folder.getId()};
        Log.i("folder", "Start loading files");
        
        if(!folder.isRoot()){
            folder.setParent(true);
            files.add(folder);
        }
        Cursor cursor=database.query(Database.FILE_TABLE, columns, "parent_id=?", args, null, null, "type, name");
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            if(cursor.getString(3).equals(FileDAO.TYPE)){
                files.add(cursorToFile(cursor));
            }else if(cursor.getString(3).equals(FolderDAO.TYPE)){
                files.add(cursorToFolder(cursor));
            }
            cursor.moveToNext();
        }
       
        cursor.close();
        return files;
    }
        
    private FileDAO cursorToFile(Cursor cursor) {
        FileDAO file=new FileDAO();
        file.setId(cursor.getString(0));
        file.setParentId(cursor.getString(1));
        file.setName(cursor.getString(2));
        file.setUrl(cursor.getString(4));
        file.setParentUrl(cursor.getString(5));
        file.setEnding(cursor.getString(6));
        return file;
    }
    
    private FolderDAO cursorToFolder(Cursor cursor) {
        FolderDAO file=new FolderDAO();
        file.setId(cursor.getString(0));
        file.setParentId(cursor.getString(1));
        file.setName(cursor.getString(2));
        file.setUrl(cursor.getString(4));
        file.setParentUrl(cursor.getString(5));
        return file;
    }
    
}
