/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package android.via.dropfolder.service;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 *
 * @author venca
 */
class Database extends SQLiteOpenHelper {

    private static final String DATABASE_NAME="dropfolder";
    
    public static final String FILE_TABLE="files";
    
    public static final String FILE_ID="id";
    public static final String FILE_PARENT_ID="parent_id";
    public static final String FILE_NAME="name";
    public static final String FILE_TYPE="type";
    public static final String FILE_URL="url";
    public static final String FILE_PARENT_URL="parent_url";
    public static final String FILE_ENDING="ending";
    
    public Database(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i("folder", "start creating database");
        createFileTable(db);
        Log.i("folder", "database was created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+FILE_TABLE);
        Log.i("folder", "file table was deleted");
        onCreate(db);
    }
    
    private void createFileTable(SQLiteDatabase db){
        db.execSQL("CREATE TABLE IF NOT EXISTS "+FILE_TABLE
                + "("
                + FILE_ID+" TEXT PRIMARY KEY,"
                + FILE_PARENT_ID+" TEXT DEFAULT NULL REFERENCES "+FILE_TABLE+"("+FILE_ID+") "
                + "ON UPDATE CASCADE "
                + "ON DELETE CASCADE,"
                + FILE_NAME+" TEXT NOT NULL,"
                + FILE_TYPE+" TEXT NOT NULL,"
                + FILE_URL+" TEXT NOT NULL,"
                + FILE_PARENT_URL+" TEXT NOT NULL,"
                + FILE_ENDING+" TEXT NOT NULL"
                + ")");
        Log.i("folder", "file table was created");
    }
}
