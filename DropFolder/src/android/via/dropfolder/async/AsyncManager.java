/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package android.via.dropfolder.async;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.via.dropfolder.adapter.AsyncAdapter;
import org.apache.http.StatusLine;

/**
 *
 * @author venca
 */
abstract public class AsyncManager extends AsyncTask<String, Integer, String> {
        
    protected Context context;
    protected AsyncAdapter<Integer, String> adapter;
    protected StatusLine status;     
    
    protected static final int STATUS_OK=200;
    protected static final int STATUS_CREATED=201;
    protected static final int STATUS_NO_CONTENT=204;
    
    protected static final int STATUS_BAD_REQUEST=400;
    protected static final int STATUS_UNAUTHORIZED=401;
    protected static final int STATUS_FORBIDDEN=403;
    protected static final int STATUS_NOT_FOUND=404;
    protected static final int STATUS_CONFLICT=409;
    
    protected static final int STATUS_INTERNAL_ERROR=500;
    
    public AsyncManager(Context context){
        this.context=context;
    }
    
    public AsyncManager(Context context, AsyncAdapter adapter){
        this.adapter=adapter;
        this.context=context;
    }
    
    public void setAdapter(AsyncAdapter adapter){
        this.adapter=adapter;
    }
    
    public boolean isNetworkAvailable(Context c) {
        ConnectivityManager connectivityManager=(ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo=connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo!=null && activeNetworkInfo.isConnected();
    }
     
    @Override
    protected void onPreExecute(){
        if(adapter!=null) adapter.onPreExecute();
    }
    
    @Override
    protected void onProgressUpdate(Integer... progress) {
         if(adapter!=null) adapter.onProgressUpdate(progress);
     }

    @Override
    protected void onPostExecute(String result) {
        if(adapter==null) return;
        adapter.onPostExecute(result);
        if(status==null) return;
        int code=status.getStatusCode();

        switch(code){
            case STATUS_OK:
                adapter.onPostExecuteStatusOk(result);
               break;
            case STATUS_CREATED:
                adapter.onPostExecuteStatusCreated(result);
               break;
            case STATUS_NO_CONTENT:
                adapter.onPostExecuteStatusNoContent(result);
                break;
            case STATUS_BAD_REQUEST:
                adapter.onPostExecuteStatusBadRequest(result);
            case STATUS_UNAUTHORIZED:
                adapter.onPostExecuteStatusUnauthorized(result);
                break;
            case STATUS_FORBIDDEN:
                adapter.onPostExecuteStatusForbidden(result);
                break;
            case STATUS_NOT_FOUND:
                adapter.onPostExecuteStatusNotFound(result);
                break;
            case STATUS_CONFLICT:
                adapter.onPostExecuteStatusConflict(result);
                break;
            case STATUS_INTERNAL_ERROR:
                adapter.onPostExecuteStatusInternalError(result);
        }

     }    

}