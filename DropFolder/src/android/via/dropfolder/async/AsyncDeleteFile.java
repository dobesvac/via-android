/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package android.via.dropfolder.async;

import android.content.Context;
import android.util.Base64;
import android.util.Log;
import android.via.dropfolder.adapter.AsyncAdapter;
import android.via.dropfolder.preferences.UserPreferences;
import java.io.IOException;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.impl.client.DefaultHttpClient;

/**
 *
 * @author venca
 */
public class AsyncDeleteFile extends AsyncManager {
    
    public AsyncDeleteFile(Context context){
        super(context);
    }
    
    public AsyncDeleteFile(Context context, AsyncAdapter adapter){
        super(context, adapter);
    }

    @Override
    protected String doInBackground(String... urls) {
        Log.i("folder", urls[0]);
        String result=null;
        HttpClient httpclient=new DefaultHttpClient();
        HttpDelete httpdelete=new HttpDelete(urls[0]);
        
        String credentials=UserPreferences.getUsername(this.context)+":"+UserPreferences.getPassword(this.context);   
        String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);  
        httpdelete.addHeader("Authorization", "Basic " + base64EncodedCredentials);
        
        try {
            HttpResponse httpResponse=httpclient.execute(httpdelete);

            this.status=httpResponse.getStatusLine();

        } catch (ClientProtocolException ex) {
            Log.e("folder", ex.getMessage());
        } catch (IOException ex) {
            Log.e("folder", ex.getMessage());
        }
        
        return result;
    }
    
}
