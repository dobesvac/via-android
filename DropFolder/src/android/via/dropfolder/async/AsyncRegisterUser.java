/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package android.via.dropfolder.async;

import android.util.Log;
import android.via.dropfolder.adapter.AsyncAdapter;
import android.via.dropfolder.crate.UserCrate;
import java.io.IOException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MIME;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;


/**
 *
 * @author venca
 */
public class AsyncRegisterUser extends AsyncManager {

    protected UserCrate crate;
    
    public AsyncRegisterUser(UserCrate crate){
        super(null);
        this.crate=crate;
    }
    
    public AsyncRegisterUser(UserCrate crate, AsyncAdapter adapter){
        super(null, adapter);
        this.crate=crate;
    }
    
    @Override
    protected String doInBackground(String... urls) {
        String result=null;
        try {
            HttpClient httpclient=new DefaultHttpClient();
            HttpPost httppost=new HttpPost(urls[0]);
                        
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setCharset(MIME.UTF8_CHARSET);
            
            builder.addTextBody("username", crate.username, ContentType.create("text/plain", MIME.UTF8_CHARSET));
            builder.addTextBody("password", crate.password, ContentType.create("text/plain", MIME.UTF8_CHARSET));
            
            httppost.setEntity(builder.build());

            HttpResponse httpResponse=httpclient.execute(httppost);
            this.status=httpResponse.getStatusLine();
 
            HttpEntity httpEntity=httpResponse.getEntity();
            result=EntityUtils.toString(httpEntity);

        } catch (ClientProtocolException ex) {
            Log.e("folder", ex.getMessage());
        } catch (IOException ex) {
            Log.e("folder", ex.getMessage());
        }
        
        return result;
    } 
    
}