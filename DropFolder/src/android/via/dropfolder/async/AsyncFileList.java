/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package android.via.dropfolder.async;

import android.content.Context;
import android.util.Base64;
import android.util.Log;
import android.via.dropfolder.preferences.UserPreferences;
import android.via.dropfolder.adapter.AsyncAdapter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

/**
 *
 * @author venca
 */
public class AsyncFileList extends AsyncManager {
    
    public AsyncFileList(Context context){
        super(context);
    }
    
    public AsyncFileList(Context context, AsyncAdapter adapter){
        super(context, adapter);
    }
 
    @Override
    protected String doInBackground(String... urls) {
        String result=null;
        try {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(urls[0]);
            
            String credentials=UserPreferences.getUsername(this.context)+":"+UserPreferences.getPassword(this.context);  
            String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);  
            httpGet.addHeader("Authorization", "Basic " + base64EncodedCredentials);
            
            HttpResponse httpResponse=httpClient.execute(httpGet);
            this.status=httpResponse.getStatusLine();
            
            HttpEntity httpEntity=httpResponse.getEntity();
            result=EntityUtils.toString(httpEntity);
            
        } catch (UnsupportedEncodingException ex) {
            Log.e("folder", "unsupported async "+ex.getMessage());
        } catch (ClientProtocolException ex) {
            Log.e("folder", "client async "+ex.getMessage());
        } catch (IOException ex) {
            Log.e("folder", "io async "+ex.getMessage());
        }
        return result;
    }   
}