/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package android.via.dropfolder.async;

import android.content.Context;
import android.util.Base64;
import android.util.Log;
import android.via.dropfolder.adapter.AsyncAdapter;
import android.via.dropfolder.crate.FileCrate;
import android.via.dropfolder.preferences.UserPreferences;
import java.io.IOException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MIME;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;


/**
 *
 * @author venca
 */
public class AsyncAddFile extends AsyncManager {

    protected FileCrate crate;
    
    public AsyncAddFile(Context context, FileCrate crate){
        super(context);
        this.crate=crate;
    }
    
    public AsyncAddFile(Context context, FileCrate folder, AsyncAdapter adapter){
        super(context, adapter);
        this.crate=folder;
    }
    
    @Override
    protected String doInBackground(String... urls) {
        String result=null;
        try {
            HttpClient httpclient=new DefaultHttpClient();
            HttpPost httppost=new HttpPost(urls[0]);
            
            String credentials=UserPreferences.getUsername(this.context)+":"+UserPreferences.getPassword(this.context);
            String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);  
            httppost.addHeader("Authorization", "Basic " + base64EncodedCredentials);
            
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setCharset(MIME.UTF8_CHARSET);
            
            builder.addBinaryBody("file", crate.file, ContentType.create(crate.mineType), crate.file.getName());
                                  
            httppost.setEntity(builder.build());

            HttpResponse httpResponse=httpclient.execute(httppost);
            this.status=httpResponse.getStatusLine();
            Log.i("folder", this.status.toString());
            HttpEntity httpEntity=httpResponse.getEntity();
            result=EntityUtils.toString(httpEntity);

        } catch (ClientProtocolException ex) {
            Log.e("folder", ex.getMessage());
        } catch (IOException ex) {
            Log.e("folder", ex.getMessage());
        }
        
        return result;
    } 
    
}