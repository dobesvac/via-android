/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package android.via.dropfolder.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.via.dropfolder.crate.UserCrate;

/**
 *
 * @author venca
 */
public class UserPreferences {
    private static final String ID="id";
    private static final String USERNAME="username";
    private static final String PASSWORD="password";
    private static final String URL="url";
    private static final String FILESYSTEM_URL="filesystem";
    
    public static void saveUserCredentials(Context context, UserCrate crate){
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putLong(ID, crate.id);
        editor.putString(USERNAME, crate.username);
        editor.putString(PASSWORD, crate.password);
        editor.putString(URL, crate.url);
        editor.putString(FILESYSTEM_URL, crate.filesystem);
        editor.commit();
    }
        
    public static boolean isLogged(Context context){
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        Long def=new Long(-1);
        Long id=sharedPrefs.getLong(ID, def);
        return !id.equals(def);
    }
    
    public static Long getId(Context context){
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPrefs.getLong(ID, new Long(-1));
    }
    
    public static String getUsername(Context context){
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPrefs.getString(USERNAME, "null");
    }
    
    public static String getPassword(Context context){
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPrefs.getString(PASSWORD, "null");
    }
    
    public static String getFileSystemUrl(Context context){
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPrefs.getString(FILESYSTEM_URL, "null");
    }
    
    public static String getUserUrl(Context context){
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPrefs.getString(URL, "null");
    }
    
    public static void clearUserCredentials(Context context){
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.remove(ID);
        editor.remove(USERNAME);
        editor.remove(PASSWORD);
        editor.remove(URL);
        editor.remove(FILESYSTEM_URL);
        editor.commit();
    }
}
