/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package android.via.dropfolder.browser;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.via.dropfolder.R;
import android.via.dropfolder.dao.FileDAO;
import android.widget.Toast;
import java.io.IOException;


public class FileBrowser {
    
    public static void openFile(Context context, FileDAO file) throws IOException {
        // Create URI
                
        Uri uri=FileProvider.getUriForFile(context, "android.via.dropfolder.files", file.getFileData());

        try{
            Intent intent=new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setDataAndType(uri, file.getContentType());
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); 
            context.startActivity(intent);
        }catch(Exception ex){
            Toast.makeText(context, R.string.no_suitable_activity, Toast.LENGTH_SHORT).show();
            Log.e("folder", ex.getMessage());
        }
    }
    
}
