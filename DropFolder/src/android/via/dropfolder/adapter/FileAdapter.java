/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package android.via.dropfolder.adapter;



import android.app.Activity;
import android.content.Context;
import android.via.dropfolder.R;
import android.via.dropfolder.dao.AbstractFileDAO;
import android.via.dropfolder.dao.FolderDAO;
import android.via.dropfolder.parser.DateParser;
import android.via.dropfolder.parser.SizeParser;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;

/**
 *
 * @author venca
 */
public class FileAdapter extends ArrayAdapter<AbstractFileDAO>  {
    
    private final Context context;
    private final List<AbstractFileDAO> values;
    private boolean isParent=false;
    
    public FileAdapter(Context context, List<AbstractFileDAO> values) {
        super(context, R.layout.row, values);
        this.context=context;
        this.values=values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View row=convertView;
        ListHolder holder=null;
        SizeParser s=null;
        DateParser d=null;
        
        if(row==null){
            LayoutInflater inflater=((Activity) context).getLayoutInflater();
            row=inflater.inflate(R.layout.row, parent, false);
            holder=new ListHolder();
            holder.name=(TextView) row.findViewById(R.id.rowName);
            holder.icon=(ImageView) row.findViewById(R.id.rowIcon);
                        
            row.setTag(holder);
        }else{
            holder=(ListHolder) row.getTag();
        }
        
        if(position==0){
            AbstractFileDAO p=values.get(position);
            if(p.isFolder()){
                FolderDAO f=(FolderDAO) p;
                if(f.isParent()) isParent=true;
            }
        }else{
            isParent=false;
        }
        
        if(isParent){
            holder.name.setText(R.string.back);
            holder.icon.setImageResource(R.drawable.back);
        }else{
            holder.name.setText(values.get(position).getName());
            if(values.get(position).isFolder()){
                holder.icon.setImageResource(R.drawable.icon);
            }else{
                holder.icon.setImageResource(fileType(values.get(position).getEnding()));
            } 
        }
        
        return row;
    }
    
    private int fileType(String type){
        type=type.toLowerCase();
        if(type.equals("pdf")){
            return R.drawable.pdf;
        }
        if(type.equals("doc")){
            return R.drawable.doc;
        }
        if(type.equals("docx")){
            return R.drawable.docx;
        }
        if(type.equals("odt")){
            return R.drawable.odt;
        }
        if(type.equals("txt")){
            return R.drawable.txt;
        }
        if(type.equals("jpg")){
            return R.drawable.jpg;
        }
        if(type.equals("gif")){
            return R.drawable.gif;
        }
        if(type.equals("png")){
            return R.drawable.png;
        }
        if(type.equals("mp3")){
            return R.drawable.mp3;
        }
        return R.drawable.file;
    }
    
    private class ListHolder {
        public TextView name;
        public ImageView icon;
    }
}
