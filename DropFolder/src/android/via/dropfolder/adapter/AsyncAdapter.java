/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package android.via.dropfolder.adapter;

import android.app.Activity;
import android.via.dropfolder.R;
import android.widget.Toast;

/**
 *
 * @author venca
 * @param <T>
 * @param <V>
 */
public abstract class AsyncAdapter<T, V> {
    
    protected Activity activity;
    
    public AsyncAdapter(Activity activity){
        this.activity=activity;
    }
    
    public void onPreExecute(){}
    public void onPostExecute(String result){}
    
    public void onPostExecuteStatusOk(String result){ /* code 200 */
        Toast.makeText(activity, "200 Ok", Toast.LENGTH_SHORT).show();
    }
    
    public void onPostExecuteStatusCreated(String result){ /* code 201 */
        Toast.makeText(activity, "201 Created", Toast.LENGTH_SHORT).show();
    }
    
    public void onPostExecuteStatusNoContent(String result){ /* code 204 */
        Toast.makeText(activity, "204 No Content", Toast.LENGTH_SHORT).show();
    } 
    
    public void onPostExecuteStatusBadRequest(String result){ /* code 400 */
        Toast.makeText(activity, "400 Bad Request", Toast.LENGTH_SHORT).show();
    }
    
    public void onPostExecuteStatusUnauthorized(String result){ /* code 401 */
        Toast.makeText(activity, R.string.unauthorized, Toast.LENGTH_SHORT).show();
    } 
    
    public void onPostExecuteStatusForbidden(String result){ /* code 403 */
        Toast.makeText(activity, "403 Forbidden", Toast.LENGTH_SHORT).show();
    } 
    
    public void onPostExecuteStatusNotFound(String result){ /* code 404 */
        Toast.makeText(activity, "404 Not Found", Toast.LENGTH_SHORT).show();
    } 
    
    public void onPostExecuteStatusConflict(String result){ /* code 409 */
        Toast.makeText(activity, "409 Conflict", Toast.LENGTH_LONG).show();
    }
    
    public void onPostExecuteStatusInternalError(String result){ /* code 500 */
        Toast.makeText(activity, "500 Internal Error", Toast.LENGTH_LONG).show();
    }
    
    public void onProgressUpdate(T... progress){}
}
