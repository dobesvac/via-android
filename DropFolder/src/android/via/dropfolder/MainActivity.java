package android.via.dropfolder;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.via.dropfolder.adapter.FileAdapter;
import android.via.dropfolder.controller.FileController;
import android.via.dropfolder.crate.FileCrate;
import android.via.dropfolder.dao.AbstractFileDAO;
import android.via.dropfolder.dao.FileDAO;
import android.via.dropfolder.dao.FolderDAO;
import android.via.dropfolder.dao.RootFolder;
import android.via.dropfolder.preferences.UserPreferences;
import android.via.dropfolder.service.FileService;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends Activity {
    
    private static final int CHOOSE_FILE_PATH=0;
    
    private FileService service;
    
    protected String baseUrl;
    private AbstractFileDAO fileItem;

    private ListView list;
    private TextView title;
    
    private FileController manager;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        this.baseUrl=UserPreferences.getFileSystemUrl(this);
                
        this.manager=new FileController(this);
        
        if(savedInstanceState!=null){
            restoreState(savedInstanceState);
        }else{
            manager.setActualUrl(this.baseUrl);
        }
        this.title=(TextView) this.findViewById(R.id.mainTitle);
        this.list=(ListView) this.findViewById(R.id.mainList);
        this.list.setOnItemClickListener(new OnClickList());
        this.registerForContextMenu(list);
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        if(manager.getFiles()==null){
            manager.loadFolderContentFromCache(new RootFolder(this));
        }else{
            this.title.setText(manager.getParent().getName());
            FileAdapter adapter=new FileAdapter(this, manager.getFiles());
            this.list.setAdapter(adapter);
        }
    }
        
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {   
        
        switch (item.getItemId()) {
            case R.id.menuNewFolder:
                AddFolderDialog dialog=new AddFolderDialog();
                dialog.show(this.getFragmentManager(), "addfolder");
                return true;
            case R.id.menuUploadFile:
                 
                try {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    intent.setType("file/*");     
                    startActivityForResult(intent, CHOOSE_FILE_PATH);
                } catch (ActivityNotFoundException ex) {
                    Log.e("folder", ex.getMessage());
                }
                //startActivity(intent);
                return true;
            case android.R.id.home: 
                manager.loadFolderContentFromCache(new RootFolder(this));
                return true;
            case R.id.menuRefresh:
                manager.loadFolderContentFromServer(manager.getActualUrl());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    } 
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data==null){
            return;
        }
        String filePath="";
        switch (requestCode) {
            case CHOOSE_FILE_PATH:
            if (resultCode==RESULT_OK) {
                filePath=data.getData().getPath();
            }
        }
        FileCrate crate=new FileCrate();
        crate.user=UserPreferences.getId(this);
        FolderDAO parent=manager.getParent();
        crate.parent=null;
        
        if(parent!=null){
            crate.parent=parent.getId();
        }
        crate.file=new File(filePath);
        
        Uri uri=Uri.fromFile(crate.file);
        String fileExtension=MimeTypeMap.getFileExtensionFromUrl(uri.toString());
        crate.mineType=MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension);
        
        manager.addFile(parent.getUrl(), crate, true);
    }
    
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.list_context_menu, menu);
    }
    
    @Override
    public boolean onContextItemSelected(MenuItem menu){
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menu.getMenuInfo();
        fileItem=manager.getFiles().get(info.position);
        switch(menu.getItemId()){
            case R.id.contextMenuShow:
                if(fileItem.isFolder()){
                    onClickFolder((FolderDAO) fileItem);
                }else{
                    onClickFile((FileDAO) fileItem);
                }
                return true;
            case R.id.contextMenuDelete:
                final Context context=this;
                String message=this.getString(R.string.delete_file_question);
                message=String.format(message, fileItem.getName());
                new AlertDialog.Builder(this)
                    .setMessage(message).setPositiveButton("Ano", new DialogInterface.OnClickListener(){
                        public void onClick(DialogInterface dialog, int which) {
                            if(fileItem.isFolder()){
                                manager.deleteFolder(fileItem.getUrl(), fileItem.getId(), true);
                            }else{
                                manager.deleteFile(fileItem.getUrl(), fileItem.getId(), true);
                            }
                            
                        }
                    })
                    .setNegativeButton("Ne", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {}
                    }).show();
                return true;
        }
        return super.onContextItemSelected(menu); 
    }
    
    @Override
    public void onBackPressed() {
        if(manager.getParent().isRoot()){
            moveTaskToBack(true);
        }else{
            if(service==null) service=new FileService(this);
            service.open();
            FolderDAO parent=(FolderDAO) service.getFile(manager.getParent().getParentId());
            service.close();
            manager.loadFolderContentFromCache(parent);
        }
    }

    
    @Override
    public void onSaveInstanceState(Bundle bundle){
        if(manager.getActualUrl()!=null){
            bundle.putString("actual", manager.getActualUrl());
        }
        if(this.list!=null){
            bundle.putParcelableArrayList("files", (ArrayList<? extends Parcelable>) manager.getFiles());
        }
        if(manager.getParent()!=null){
            bundle.putParcelable("current", manager.getParent());
        }
    }
    
    public FileController getAsyncFileManager(){
        return manager;
    }
    
    private void restoreState(Bundle state){
        if(state!=null){
            manager.setActualUrl(state.getString("actual"));
            manager.setParent((FolderDAO) state.getParcelable("current"));
            List<AbstractFileDAO> l=state.getParcelableArrayList("files");
            manager.setFiles(l);
        }
    }
    
    private void onClickFolder(FolderDAO detail){
        if(detail.isParent()){
            if(service==null) service=new FileService(this);
            service.open();
            FolderDAO parent=(FolderDAO) service.getFile(detail.getParentId());
            service.close();
            manager.loadFolderContentFromCache(parent);
        }else{
            manager.loadFolderContentFromCache(detail);
        }
    }
        
    private void onClickFile(FileDAO detail){
        if(detail.containsData()){
//            Toast.makeText(this, "cache", Toast.LENGTH_SHORT).show();
            manager.loadFileFromCache(detail);
        }else{
//            Toast.makeText(this, "server", Toast.LENGTH_SHORT).show();
            manager.loadFileFromServer(detail.getUrl());
        }
    }
    
    private class OnClickList implements AdapterView.OnItemClickListener {
        
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if(manager.getFiles().get(position).isFolder()){
                onClickFolder((FolderDAO) manager.getFiles().get(position));
            }else{
                onClickFile((FileDAO) manager.getFiles().get(position));
            }
        }
        
    }
    
}