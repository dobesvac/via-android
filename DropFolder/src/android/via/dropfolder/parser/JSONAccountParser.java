/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package android.via.dropfolder.parser;

import android.content.Context;
import android.util.Log;
import android.via.dropfolder.crate.UserCrate;
import android.via.dropfolder.preferences.UserPreferences;
import org.json.JSONException;

/**
 *
 * @author venca
 */
public class JSONAccountParser extends JSONParser {
    
    private final Context context;
    private final String password;
    
    public JSONAccountParser(Context context, String password,  String json) {
        super(json);
        this.context=context;
        this.password=password;
    }

    @Override
    public void parse() {
        try {
            UserCrate crate=new UserCrate();
            
            crate.id=Long.valueOf(parser.getString("id"));
            crate.username=parser.getString("username");
            crate.password=password;
            crate.url=parser.getString("url");
            crate.filesystem=parser.getString("filesystemUrl");
            
            UserPreferences.saveUserCredentials(context, crate);
        } catch (JSONException ex) {
            Log.e("folder", ex.getMessage());
        }
    }
    
}
