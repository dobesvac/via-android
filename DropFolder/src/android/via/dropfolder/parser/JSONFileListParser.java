/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package android.via.dropfolder.parser;

import android.content.Context;
import android.util.Log;
import android.via.dropfolder.dao.AbstractFileDAO;
import android.via.dropfolder.dao.FolderDAO;
import android.via.dropfolder.dao.RootFolder;
import android.via.dropfolder.service.FileService;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;

/**
 *
 * @author venca
 */
public class JSONFileListParser extends JSONFileParser {
        
    private FolderDAO parent;
    private List<AbstractFileDAO> files;
    private final FileService service;
    private final Context context;
    
    public JSONFileListParser(Context context, FileService service, String json) {
        super(context, service, null, json);
        files=new ArrayList();   
        this.service=service;
        this.context=context;
    }
    
    private void clearCache(String parent){
        service.open();
        service.deleteAllParentFiles(parent);
        service.close();
    }
    
    @Override
    public void parse() {
        try {
            String id=parser.getString("objectName");
            if(id.equals("null")){
                parent=new RootFolder(this.context);
                parent.setParent(true);
            }else{
                parent=new FolderDAO();
                parent.setId(id);
                parent.setName(parser.getString("name"));
                parent.setUrl(parser.getString("url"));
                String parentUrl=parser.getString("parentUrl");
                if(parentUrl.equals("null")) parentUrl=parser.getString("url");
                parent.setParentUrl(parentUrl);
                service.open();
                parent=(FolderDAO) service.saveOrUpdate(parent);
                service.close();
                parent.setParent(true);
            }
            clearCache(parent.getId());
            parseFiles();
        } catch (JSONException ex) {
            Log.e("folder", ex.getMessage());
        }
    }
    
    public List getFiles() {
        return files;
    }
    
    public FolderDAO getParent() {
        return parent;
    }
    
    private void parseFiles() throws JSONException{
        JSONArray fArray=parser.getJSONArray("data");
        for (int i = 0; i < fArray.length(); i++) {
            service.open();
            service.saveOrUpdate(readFile(parent, fArray.getJSONObject(i)));
            service.close();
        }
    }
    
}
