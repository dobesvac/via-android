/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package android.via.dropfolder.parser;

import android.util.Log;
import java.io.InputStream;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author venca
 */
public abstract class JSONParser {
    
    protected JSONObject parser;
    
    public JSONParser(String json){
        InputStream in=null;
        try {
            parser=new JSONObject(json);
        } catch (JSONException ex) {
            Log.e("folder", ex.getMessage());
        }
    }
    
    abstract public void parse();
    
}
