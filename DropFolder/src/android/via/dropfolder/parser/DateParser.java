/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package android.via.dropfolder.parser;

import android.util.Log;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author venca
 */
public class DateParser {
    
    private Date date;
    private String format;
    
    public DateParser(String date) {
        this.format="yyyy-MM-dd";
        this.date=setDateFromString(date);
    }
    
    public DateParser(String date, String format) {
        this.format=format;
        this.date=setDateFromString(date);
    }
    
    public DateParser(Date date) {
        this.format="yyyy-MM-dd";
        this.date=date;
    }
    
    private Date setDateFromString(String sDate){
        try {
            return new SimpleDateFormat(format).parse(sDate);
        } catch (ParseException ex) {
            Log.e("todolist", ex.getMessage());
        }
        return null;
    }
    
    public String getDateString(){
        return parseDate(null);
    }
    
    public String getDateString(String format){
        return parseDate(format);
    }
    
    public Date getDateObject(){
        return date;
    }
    
    private String parseDate(String format){
        if(date==null){
            return "";
        }
        if(format==null){
            format=this.format;
        }
        return new SimpleDateFormat(format).format(date);
    }
    
}
