/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package android.via.dropfolder.parser;

import android.content.Context;
import android.util.Base64;
import android.util.Log;
import android.via.dropfolder.dao.AbstractFileDAO;
import android.via.dropfolder.dao.FileDAO;
import android.via.dropfolder.dao.FolderDAO;
import android.via.dropfolder.service.FileService;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author venca
 */
public class JSONFileParser extends JSONParser {
        
    private AbstractFileDAO file;
    private FolderDAO parent;
    private final FileService service;
    private final Context context;
    
    public JSONFileParser(Context context, FileService service, FolderDAO parent, String json) {
        super(json);  
        this.service=service;
        this.context=context;
        this.parent=parent;
    }
    
    @Override
    public void parse() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    public void parseFolderContent() {
        try {
            file=readFile(parent, parser);
            service.open();
            service.saveOrUpdate(file);
            service.close();
            
        } catch (JSONException ex) {
            Log.e("folder", ex.getMessage());
        }
    }
    
    public void parseFile(){
        try {
            FileDAO tmp=(FileDAO) readFile(parent, parser);
            String data=parser.getString("data");
            byte[] bFile=Base64.decode(data, Base64.NO_WRAP);

            File outputFile=new File(context.getCacheDir(), tmp.getName());
            FileOutputStream stream=new FileOutputStream(outputFile.getAbsoluteFile());
            stream.write(bFile);
            tmp.setFileData(outputFile);
            tmp.setContentType(parser.getString("contentType"));
            file=tmp;
        } catch (JSONException ex) {
            Log.e("folder", ex.getMessage());
        } catch (IOException ex) {
            Log.e("folder", ex.getMessage());
        }
    }
     
    public AbstractFileDAO getFile(){
        return this.file;
    }
    
    protected AbstractFileDAO readFile(FolderDAO parent, JSONObject file) throws JSONException {
        AbstractFileDAO f=null;
        if(file.getString("type").equals(FileDAO.TYPE)){
            f=new FileDAO();
        }else if(file.getString("type").equals(FolderDAO.TYPE)){
            f=new FolderDAO();
        }else{
            throw new JSONException("Neočekávaný typ souboru");
        }
        String name=file.getString("name");
        f.setId(file.getString("objectName"));
        f.setParentId(parent.getId());
        f.setName(name);
        f.setUrl(file.getString("url"));
        f.setParentUrl(parent.getUrl());
        f.setEnding(parseEnding(name));
        
        return f;
    }
    
    protected String parseEnding(String file){
        if(file.contains(".")){
            String[] s=file.split("\\.");
            return s[s.length-1];
        }
        return "";
    }   
    
}
