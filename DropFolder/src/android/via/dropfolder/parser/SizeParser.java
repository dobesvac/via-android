/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package android.via.dropfolder.parser;

/**
 *
 * @author venca
 */
public class SizeParser {
    
    private Long bytes;
    
    public SizeParser(Long bytes){
        this.bytes=bytes;
    }
    
    public String getReadable(){
        if(bytes==null){
            return "";
        }
        Long unit=new Long(1024);
        if (bytes.compareTo(unit)==0){
            return this.bytes+" B";
        }
        int exp=(int) (Math.log(bytes) / Math.log(unit));
        String pre = ("kMGTPE").charAt(exp-1)+"";
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }

}

